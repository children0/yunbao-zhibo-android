package com.yunbao.common.utils;

import android.content.Context;
import android.content.Intent;

import com.yunbao.common.CommonAppConfig;
import com.yunbao.common.CommonAppContext;
import com.yunbao.common.Constants;
import com.yunbao.common.activity.AbsActivity;
import com.yunbao.common.http.CommonHttpUtil;
import com.yunbao.common.http.HttpCallback;

// +----------------------------------------------------------------------
// | Created by Yunbao
// +----------------------------------------------------------------------
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: https://gitee.com/yunbaokeji/
// +----------------------------------------------------------------------
// | Date: 2022-02-17
// +----------------------------------------------------------------------

public class RouteUtil {
    /**
     * 启动页
     */
    public static void forwardLauncher() {
        Intent intent = new Intent();
        intent.setClassName(CommonAppConfig.PACKAGE_NAME, "com.yunbao.phonelive.activity.LauncherActivity");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        CommonAppContext.getInstance().startActivity(intent);
    }

    /**
     * 登录
     */
    public static void forwardLogin(Context context, String tip) {
        Intent intent = new Intent();
        intent.setClassName(CommonAppConfig.PACKAGE_NAME, "com.yunbao.main.activity.LoginActivity");
        intent.putExtra(Constants.TIP, tip);
        context.startActivity(intent);
    }

    /**
     * 登录
     */
    public static void forwardLogin(Context context) {
        Intent intent = new Intent();
        intent.setClassName(CommonAppConfig.PACKAGE_NAME, "com.yunbao.main.activity.LoginActivity");
        context.startActivity(intent);
    }

    /**
     * 登录过期
     */
    public static void forwardLoginInvalid(String tip) {
        Intent intent = new Intent();
        intent.setClassName(CommonAppConfig.PACKAGE_NAME, "com.yunbao.main.activity.LoginInvalidActivity");
        intent.putExtra(Constants.TIP, tip);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        CommonAppContext.getInstance().startActivity(intent);
    }


    /**
     * 跳转到个人主页
     */
    public static void forwardUserHome(Context context, String toUid) {
        forwardUserHome(context, toUid, false, null);
    }

    /**
     * 跳转到个人主页
     */
    public static void forwardUserHome(Context context, String toUid, boolean fromLiveRoom, String fromLiveUid) {
        Intent intent = new Intent();
        intent.setClassName(CommonAppConfig.PACKAGE_NAME, "com.yunbao.main.activity.UserHomeActivity");
        intent.putExtra(Constants.TO_UID, toUid);
        intent.putExtra(Constants.FROM_LIVE_ROOM, fromLiveRoom);
        intent.putExtra(Constants.LIVE_UID, fromLiveUid);
        context.startActivity(intent);
    }

    /**
     * 跳转到充值页面
     */
    public static void forwardMyCoin(Context context) {
        Intent intent = new Intent();
        intent.setClassName(CommonAppConfig.PACKAGE_NAME, "com.yunbao.main.activity.MyCoinActivity");
        context.startActivity(intent);
    }



    /**
     * 付费内容详情页面
     */
    public static void forwardPayContentDetail(Context context, String goodsId) {
        Intent intent = new Intent();
        intent.setClassName(CommonAppConfig.PACKAGE_NAME, "com.yunbao.mall.activity.PayContentDetailActivity");
        intent.putExtra(Constants.MALL_GOODS_ID, goodsId);
        context.startActivity(intent);
    }


    /**
     * 跳转到认证页面
     */
    public static void forwardAuth(AbsActivity activity, int requestCode) {
        Intent intent = new Intent();
        intent.setClassName(CommonAppConfig.PACKAGE_NAME, "com.yunbao.main.activity.AuthActivity");
        activity.startActivityForResult(intent, requestCode);
    }


    /**
     * 跳转到家族页面
     */
    public static void forwardFamily(AbsActivity activity, int requestCode) {
        Intent intent = new Intent();
        intent.setClassName(CommonAppConfig.PACKAGE_NAME, "com.yunbao.main.activity.FamilyApplyActivity");
        activity.startActivityForResult(intent, requestCode);
    }

    /**
     * 跳转到app内部的指定类名的activity
     *
     * @param context   出发的Activity
     * @param className 目标Activity的全类名
     */
    public static void forward(Context context, String className) {
        Intent intent = new Intent();
        intent.setClassName(CommonAppConfig.PACKAGE_NAME, className);
        context.startActivity(intent);
    }


    /**
     * 分享商品到动态
     */
    public static void forwardActivePub(Context context, String goodsId) {
        Intent intent = new Intent();
        intent.setClassName(CommonAppConfig.PACKAGE_NAME, "com.yunbao.main.activity.ActivePubActivity");
        intent.putExtra(Constants.MALL_GOODS_ID, goodsId);
        context.startActivity(intent);
    }


    /**
     * 分享商品到好友
     */
    public static void forwardShareGoods(Context context, String goodsId) {
        Intent intent = new Intent();
        intent.setClassName(CommonAppConfig.PACKAGE_NAME, "com.yunbao.main.activity.GoodsShareFriendActivity");
        intent.putExtra(Constants.MALL_GOODS_ID, goodsId);
        context.startActivity(intent);
    }


}
