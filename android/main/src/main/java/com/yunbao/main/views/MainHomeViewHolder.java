package com.yunbao.main.views;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.yunbao.common.custom.HomeIndicatorTitle;
import com.yunbao.common.utils.ScreenDimenUtil;
import com.yunbao.common.utils.WordUtil;
import com.yunbao.main.R;

import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.ColorTransitionPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.SimplePagerTitleView;
// +----------------------------------------------------------------------
// | Created by Yunbao
// +----------------------------------------------------------------------
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: https://gitee.com/yunbaokeji/
// +----------------------------------------------------------------------
// | Date: 2022-02-17
// +----------------------------------------------------------------------
/**
 * MainActivity 首页
 */

public class MainHomeViewHolder extends AbsMainHomeParentViewHolder {

    private MainHomeLiveViewHolder mLiveViewHolder;
    private int mStatusBarHeight;
    private View mAppBarChildView;
    private View mCover;
    private int mChangeHeight;


    public MainHomeViewHolder(Context context, ViewGroup parentView) {
        super(context, parentView);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.view_main_home;
    }

    @Override
    public void init() {
        setStatusHeight();
        super.init();
        mStatusBarHeight = ScreenDimenUtil.getInstance().getStatusBarHeight();
        mAppBarChildView = findViewById(R.id.app_bar_child_view);
        mCover = findViewById(R.id.cover);
        mAppBarLayout.post(new Runnable() {
            @Override
            public void run() {
                mChangeHeight = mAppBarLayout.getHeight() - mStatusBarHeight;
            }
        });
    }

    public void onAppBarOffsetChanged(float verticalOffset) {
        if (mChangeHeight != 0 && mCover != null) {
            float rate = verticalOffset / mChangeHeight;
            mCover.setAlpha(rate);
        }
    }

    @Override
    protected void loadPageData(int position) {
        if (mAppBarChildView != null) {
            mAppBarChildView.setMinimumHeight(position == 1 ? 0 : mStatusBarHeight);
        }
        if (mViewHolders == null) {
            return;
        }
        AbsMainHomeChildViewHolder vh = mViewHolders[position];
        if (vh == null) {
            if (mViewList != null && position < mViewList.size()) {
                FrameLayout parent = mViewList.get(position);
                if (parent == null) {
                    return;
                }
                if (position == 0) {
                    mLiveViewHolder = new MainHomeLiveViewHolder(mContext, parent);
                    vh = mLiveViewHolder;
                }
                if (vh == null) {
                    return;
                }
                mViewHolders[position] = vh;
                vh.addToParent();
                vh.subscribeActivityLifeCycle();
            }
        }
        if (vh != null) {
            vh.loadData();
        }
    }

    @Override
    protected int getPageCount() {
        return 1;
    }

    @Override
    protected String[] getTitles() {
        return new String[]{
                WordUtil.getString(R.string.live),
        };
    }


    @Override
    protected IPagerTitleView getIndicatorTitleView(Context context, String[] titles, final int index) {
        HomeIndicatorTitle indicatorTitle = new HomeIndicatorTitle(mContext);
        SimplePagerTitleView simplePagerTitleView = new ColorTransitionPagerTitleView(context);
        simplePagerTitleView.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT));
        simplePagerTitleView.setNormalColor(ContextCompat.getColor(mContext, R.color.gray1));
        simplePagerTitleView.setSelectedColor(ContextCompat.getColor(mContext, R.color.textColor));
        simplePagerTitleView.setText(titles[index]);
        simplePagerTitleView.setTextSize(18);
        simplePagerTitleView.getPaint().setFakeBoldText(true);
        indicatorTitle.addView(simplePagerTitleView);
        indicatorTitle.setTitleView(simplePagerTitleView);
        indicatorTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mViewPager != null) {
                    mViewPager.setCurrentItem(index);
                }
            }
        });
        return indicatorTitle;
    }


}
