package com.yunbao.live.dialog;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.yunbao.common.CommonAppConfig;
import com.yunbao.common.Constants;
import com.yunbao.common.activity.AbsActivity;
import com.yunbao.common.bean.LevelBean;
import com.yunbao.common.bean.UserBean;
import com.yunbao.common.dialog.AbsDialogFragment;
import com.yunbao.common.glide.ImgLoader;
import com.yunbao.common.http.HttpCallback;
import com.yunbao.common.utils.CommonIconUtil;
import com.yunbao.common.utils.RouteUtil;
import com.yunbao.common.utils.WordUtil;
import com.yunbao.live.R;
import com.yunbao.live.activity.LiveActivity;
import com.yunbao.live.http.LiveHttpConsts;
import com.yunbao.live.http.LiveHttpUtil;
import com.yunbao.live.utils.LiveTextRender;
// +----------------------------------------------------------------------
// | Created by Yunbao
// +----------------------------------------------------------------------
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: https://gitee.com/yunbaokeji/
// +----------------------------------------------------------------------
// | Date: 2022-02-17
// +----------------------------------------------------------------------
/**
 * 直播间个人资料弹窗
 */

public class LiveUserDialogFragment extends AbsDialogFragment implements View.OnClickListener {

    private ViewGroup mBottomContainer;
    private ImageView mAvatar;
    private ImageView mLevelAnchor;
    private ImageView mLevel;
    private TextView mLevelText;
    private TextView mLevelAnchorText;
    private ImageView mSex;
    private TextView mName;
    private TextView mID;
    private TextView mSign;
    private TextView mConsume;//消费
    private TextView mVotes;//收入
    private TextView mConsumeTip;
    private TextView mVotesTip;
    private String mLiveUid;
    private String mStream;
    private String mToUid;
    private String mToName;//对方的名字
    private UserBean mUserBean;

    @Override
    protected int getLayoutId() {
        return R.layout.dialog_live_user;
    }

    @Override
    protected int getDialogStyle() {
        return R.style.dialog2;
    }

    @Override
    protected boolean canCancel() {
        return true;
    }

    @Override
    protected void setWindowAttributes(Window window) {
        WindowManager.LayoutParams params = window.getAttributes();
        window.setWindowAnimations(R.style.bottomToTopAnim);
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        params.gravity = Gravity.BOTTOM;
        window.setAttributes(params);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle == null) {
            return;
        }
        mLiveUid = bundle.getString(Constants.LIVE_UID);
        mToUid = bundle.getString(Constants.TO_UID);
        if (TextUtils.isEmpty(mLiveUid) || TextUtils.isEmpty(mToUid)) {
            return;
        }
        mStream = bundle.getString(Constants.STREAM);
        mBottomContainer = mRootView.findViewById(R.id.bottom_container);
        mAvatar = (ImageView) mRootView.findViewById(R.id.avatar);
        mLevelAnchor = (ImageView) mRootView.findViewById(R.id.anchor_level);
        mLevel = (ImageView) mRootView.findViewById(R.id.level);
        mLevelText = mRootView.findViewById(R.id.level_text);
        mLevelAnchorText = mRootView.findViewById(R.id.level_anchor_text);
        mSex = (ImageView) mRootView.findViewById(R.id.sex);
        mName = (TextView) mRootView.findViewById(R.id.name);
        mID = (TextView) mRootView.findViewById(R.id.id_val);
        mSign = mRootView.findViewById(R.id.sign);
        mConsume = (TextView) mRootView.findViewById(R.id.consume);
        mVotes = (TextView) mRootView.findViewById(R.id.votes);
        mConsumeTip = (TextView) mRootView.findViewById(R.id.consume_tip);
        mVotesTip = (TextView) mRootView.findViewById(R.id.votes_tip);
        if(!mToUid.equals(CommonAppConfig.getInstance().getUid())){
            View bottomView = LayoutInflater.from(mContext).inflate(R.layout.dialog_live_user_bottom_1, mBottomContainer, false);;
            mBottomContainer.addView(bottomView);
            View btnHomePage = bottomView.findViewById(R.id.btn_home_page);
            if (btnHomePage != null) {
                btnHomePage.setOnClickListener(this);
            }
        }
        loadData();
    }



    private void loadData() {
        LiveHttpUtil.getLiveUser(mToUid, mLiveUid, new HttpCallback() {
            @Override
            public void onSuccess(int code, String msg, String[] info) {
                if (code == 0 && info.length > 0) {
                    showData(info[0]);
                }
            }
        });
    }

    private void showData(String data) {
        JSONObject obj = JSON.parseObject(data);
        mUserBean = JSON.toJavaObject(obj, UserBean.class);
        CommonAppConfig appConfig = CommonAppConfig.getInstance();
        mID.setText("ID:"+mUserBean.getId());
        mToName = obj.getString("user_nicename");
        mName.setText(mToName);
        ImgLoader.displayAvatar(mContext, obj.getString("avatar"), mAvatar);
        int levelAnchor = obj.getIntValue("level_anchor");
        int level = obj.getIntValue("level");
        mSign.setText(obj.getString("signature"));
        LevelBean anchorLevelBean = appConfig.getAnchorLevel(obj.getIntValue("level_anchor"));
        if (anchorLevelBean != null) {
            ImgLoader.display(mContext, anchorLevelBean.getBgIcon(), mLevelAnchor);
        }
        LevelBean levelBean = appConfig.getLevel(obj.getIntValue("level"));
        if (levelBean != null) {
            ImgLoader.display(mContext, levelBean.getBgIcon(), mLevel);
        }
        mLevelAnchorText.setText(String.valueOf(levelAnchor));
        mLevelText.setText(String.valueOf(level));
        mSex.setImageResource(CommonIconUtil.getSexIcon(obj.getIntValue("sex")));
        mConsume.setText(LiveTextRender.renderLiveUserDialogData(obj.getLongValue("consumption")));
        mVotes.setText(LiveTextRender.renderLiveUserDialogData(obj.getLongValue("votestotal")));
        mConsumeTip.setText(WordUtil.getString(R.string.live_user_send) + appConfig.getCoinName());
        mVotesTip.setText(WordUtil.getString(R.string.live_user_get) + appConfig.getVotesName());


    }





    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i != R.id.btn_close && i != R.id.btn_home_page) {
            if (!((AbsActivity) mContext).checkLogin()) {
                dismiss();
                return;
            }
        }
        if (i == R.id.btn_close) {
            dismiss();

        } else if (i == R.id.btn_home_page) {
            forwardHomePage();
        }
    }


    /**
     * 跳转到个人主页
     */
    private void forwardHomePage() {
        dismiss();
        RouteUtil.forwardUserHome(mContext, mToUid, true, mLiveUid);
    }



    @Override
    public void onDestroy() {
        LiveHttpUtil.cancel(LiveHttpConsts.GET_LIVE_USER);
        if (mAvatar != null) {
            ImgLoader.clear(mContext, mAvatar);
            mAvatar.setImageDrawable(null);
        }
        mAvatar = null;
        super.onDestroy();
    }


}
