package com.yunbao.phonelive;


import com.mob.MobSDK;
import com.tencent.rtmp.TXLiveBase;
import com.yunbao.common.CommonAppContext;


// +----------------------------------------------------------------------
// | Created by Yunbao
// +----------------------------------------------------------------------
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: https://gitee.com/yunbaokeji/
// +----------------------------------------------------------------------
// | Date: 2022-02-17
// +----------------------------------------------------------------------

public class AppContext extends CommonAppContext {

    @Override
    public void onCreate() {
        super.onCreate();
        initSdk();
    }

    public static void initSdk() {
        CommonAppContext context = CommonAppContext.getInstance();
        //腾讯云直播鉴权url
        String liveLicenceUrl = "https://license.vod2.myqcloud.com/xxxxx/xxx/xxxxxxxxxxxxxxxxxxx/xxxxxx.license";
        //腾讯云直播鉴权key
        String liveKey = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
        TXLiveBase.getInstance().setLicence(context, liveLicenceUrl, liveKey);
        //初始化ShareSdk
        MobSDK.init(context, "xxxxxxxxxxxx", "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
    }



}
