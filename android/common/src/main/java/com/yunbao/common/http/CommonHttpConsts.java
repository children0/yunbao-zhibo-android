package com.yunbao.common.http;

// +----------------------------------------------------------------------
// | Created by Yunbao
// +----------------------------------------------------------------------
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: https://gitee.com/yunbaokeji/
// +----------------------------------------------------------------------
// | Date: 2022-02-17
// +----------------------------------------------------------------------


public class CommonHttpConsts {
    public static final String GET_CONFIG = "getConfig";
    public static final String GET_QQ_LOGIN_UNION_ID = "getQQLoginUnionID";
    public static final String SET_ATTENTION = "setAttention";
    public static final String GET_ALI_ORDER = "getAliOrder";
    public static final String GET_WX_ORDER = "getWxOrder";
    public static final String DOWNLOAD_GIF = "downloadGif";
    public static final String GET_BALANCE = "getBalance";
    public static final String CHECK_TOKEN_INVALID = "checkTokenInvalid";
    public static final String GET_BEAUTY_VALUE = "getBeautyValue";
    public static final String GET_UPLOAD_INFO = "getUploadInfo";
}
