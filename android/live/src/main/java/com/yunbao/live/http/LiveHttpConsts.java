package com.yunbao.live.http;

// +----------------------------------------------------------------------
// | Created by Yunbao
// +----------------------------------------------------------------------
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: https://gitee.com/yunbaokeji/
// +----------------------------------------------------------------------
// | Date: 2022-02-17
// +----------------------------------------------------------------------

public class LiveHttpConsts {
    public static final String GET_COIN = "getCoin";
    public static final String GET_USER_LIST = "getUserList";
    public static final String ROOM_CHARGE = "roomCharge";
    public static final String TIME_CHARGE = "timeCharge";
    public static final String GET_LIVE_END_INFO = "getLiveEndInfo";
    public static final String GET_LIVE_USER = "getLiveUser";
    public static final String SEND_DANMU = "sendDanmu";
    public static final String CHECK_LIVE = "checkLive";
    public static final String ENTER_ROOM = "enterRoom";
    public static final String GET_GIFT_LIST = "getGiftList";
    public static final String SEND_GIFT = "sendGift";
    public static final String CREATE_ROOM = "createRoom";
    public static final String CHANGE_LIVE = "changeLive";
    public static final String STOP_LIVE = "stopLive";
    public static final String GET_LIVE_INFO = "getLiveInfo";

}
